#!/bin/bash

# Before running this script 
# copy "env.sample" to ".env"
# cp env.sample .env
# And configure your variables

cp sample-docker-compose.yml docker-compose.yml
docker-compose up -d
